public class Coins {
	public int hundreds;
	public int twenties;
	
	public Coins(int h, int t){
		this.hundreds=h;
		this.twenties=t;
	}
	public int getSum(){
		return 100*hundreds+20*twenties;
	}
	public int getTwenties(){
		return twenties;
	}
	public int getHundreds(){
		return hundreds;
	}
	public void addCoins(int h, int t){
		this.hundreds=this.hundreds+h;
		this.twenties=this.twenties+t;
	}
	public void reduceCoins(int h, int t){
		this.hundreds=this.hundreds-h;
		this.twenties=this.twenties-t;
	}
	public void coinsRecount(){
		if(twenties>40){//balance amount of 100s and 20s coins // if 20s coins>40
			hundreds+=4; //transform 20 20s coins to 4 100s coins 
			twenties-=20;
		}else if(hundreds==0 && twenties>5){ 
			hundreds=1;
			twenties-=5;
		}else if(twenties==0 && hundreds>0){
			hundreds-=1;
			twenties=5;
		}
	}
}