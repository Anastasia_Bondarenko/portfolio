import java.util.Random;
class DynamicCardArray{
	private Card[] cards;
	private int next;
	
	public DynamicCardArray(){
		cards = new Card[2];
		next=0;
	}
	public String toString(){
		String string="";
		for(int count=next-1;count>=0;count--){
			string+=cards[count]+"; ";
		}
		return string;
	}
	public int handLength(){
		return this.next;
	}
	//dealer shows first card
	public String firstCard(){
		return cards[0]+"; HIDDEN"+'\n'+"First Card Total: "+cards[0].getTotal();
	}
	public int countTotal(){ //Cards total
		int total=0;
		int ace = 0;
		for (Card card : this.cards ){
			if(card.getValue()==Value.ACE){ 
				ace++;
			}else{
				total+=card.getTotal(); 
			}
		}
		//values from Aces
		for(int elevens=ace, ones=0;elevens>=0;elevens--,ones++){ 
			if(elevens==0){
				total+=ones; // no effect on total if ace=0
				break;
			}
			if(total+11*elevens+ones <= 21){
				total+=11*elevens+ones;
				break;
			}
		}				
		
		return total;
	}
	public void addCard(Card card){ 
		if(next>=cards.length){
			growArray();
		}
		cards[next] = card;
		next++;
	}
	public void growArray(){
		Card[] newAr=new Card[cards.length+1];
		for(int count=0;count<next;count++){
			newAr[count]=cards[count];
		}
		cards=newAr;
	}
	/*this method is to create a deck of cards*/
	public void deck(){
		this.cards=new Card[52];
		int index=0;
		for (Suit s: Suit.values()) {
			for (Value v: Value.values()) {
				cards[index]=new Card(v,s);
				index++;
			}
		}
		this.next = cards.length;
		
	}
	/*this method will randomly pick a card from deck*/ 
	public Card takeOut(){ //--Card
		Random rand = new Random();
		int pileLength=this.next;
		int index=rand.nextInt(pileLength);
		Card newCard=cards[index];
		sort(index); //call sort index
		return newCard;
		
	}
	/*Shuffle the cards*/
	public void shuffle(){
		Random ind = new Random();
		int temp=next; 
		Card[] newDeck=new Card[cards.length];
		for(int count=0; count<newDeck.length; count++){
			int index = ind.nextInt(next);
			newDeck[count]=this.cards[index];
			sort(index);
		}
		this.cards=newDeck;
		this.next=temp; //remain the used deck length
	}
	/*Sort the cards*/
	public void sort(int index){
		this.cards[index]=this.cards[next-1];
		this.next--;
	}
	
}