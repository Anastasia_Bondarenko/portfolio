import java.util.Scanner;
class Blackjack_Application{
	public static void main(String[]args){
		Scanner options = new Scanner(System.in);
		Coins coins=new Coins(5,8);
		int rounds=1;
		
		DynamicCardArray player=new DynamicCardArray(); //player cards at hand
		DynamicCardArray dealer=new DynamicCardArray(); //dealer cards at hand
		DynamicCardArray deck = new DynamicCardArray(); // deck of cards
		deck.deck(); //initialize the cards inside deck
		deck.shuffle(); //shuffle cards
		
		while(gameOver(rounds,deck,coins)){
			boolean BJ=false; //boolean blackjack = false; looks if the player has 21
			player=new DynamicCardArray(); //player cards at hand
		    dealer=new DynamicCardArray(); //dealer cards at hand
			begin(player,dealer,deck,rounds); //initialize cards values //print messages, cards at hand, total and coins
			
			//Player will place a bet
			Bet bet=placeBet(coins); //validation
		
			//blackjack
			if(blackjack(player.countTotal())){
				 BJ=true; //player immediately has a blackjack // paid 2:1
			}else{
				//Here the player will decide their options
				playerOptions(coins, bet, player, dealer, deck);
			}
			
			endRound(player,dealer,bet,coins,BJ);
			coins.coinsRecount();
			rounds++;
		}
	}
	public static boolean burst (int x){
		return x>21;
	}
	public static boolean blackjack (int x){
		return x==21;
	}
	public static boolean gameOver(int rounds, DynamicCardArray deck,Coins coins){
		if(coins.getSum()==0){
			System.out.println("===================="+'\n'+"YOU LOST ALL YOUR COINS");
			return false;
		}
		if(deck.handLength()<10){
			deck.deck(); // renew the deck to avoid OutOfBounds
		}
		return rounds<11;
	}
	public static void begin(DynamicCardArray player,DynamicCardArray dealer, DynamicCardArray deck, int rounds){ // regular hit
		player.addCard(deck.takeOut());
		player.addCard(deck.takeOut());
		dealer.addCard(deck.takeOut());
		dealer.addCard(deck.takeOut());
		//displays the player's cards and the dealer's fisrt card //method
		System.out.println('\n'+"=====ROUND "+rounds+"=====");
		System.out.println("DEALER: "+dealer.firstCard());
		System.out.println("PLAYER: "+player+'\n'+"Your total: "+player.countTotal());
	}
	//The player place their bet //Renew bet
	public static Bet placeBet(Coins coins){ 
		Scanner options = new Scanner(System.in);
		System.out.println("Place your bet, separate amount of coins by \";\" ");
		System.out.println("Hundreds: "+coins.getHundreds()+";"+" Twenties: "+coins.getTwenties()+";"+" Total: "+coins.getSum());
		String numberOfCoins=options.next();
		invalidBet(numberOfCoins.split(";",2));//validation
		Bet bet=new Bet(numberOfCoins.split(";",2)); 
		while(checkCoins(coins,bet)){ //make sure the input does not exceed
			numberOfCoins=options.next();
			invalidBet(numberOfCoins.split(";",2));//validation
			bet=new Bet(numberOfCoins.split(";",2));
		}
		return bet;
	}
	public static void invalidBet(String[] coins){ //checks if the input in the bet is correct
		try{ 
			int h=Integer.parseInt(coins[0]); //hundreds
			int t=Integer.parseInt(coins[1]); //twenties
		}
		catch(NumberFormatException e){
			System.out.println("The input has to be 2 numbers separated by \";\" ");
			throw new NumberFormatException(" WRONG INPUT FORMAT ");
		}
		catch(ArrayIndexOutOfBoundsException e){
			System.out.println("Missing number(s)!");
			throw new NumberFormatException(" MUST BE AT LEAST 2 NUMBERS ");
		}
	}
	public static void playerOptions(Coins coins, Bet bet,DynamicCardArray player, DynamicCardArray dealer, DynamicCardArray deck){
		Scanner options = new Scanner(System.in);
		System.out.println('\n'+"Choose an option:"+'\n'+"s for Stand; h for Hit; d for DoubleDown");
		String option = options.next();
		char op = option.charAt(0);
		while( op!='s'&& op!='h' && op!='d'){
			System.out.println('\n'+"Choose an option:"+'\n'+"s for Stand; h for Hit; d for DoubleDown");
			option = options.next();
			op = option.charAt(0);
		}
		if(op=='d'){ 
			if(!bet.doubleDown(coins)){
				while( op!='s'&& op!='h'){
					System.out.println('\n'+"Choose an option:"+'\n'+"s for Stand; h for Hit");
					option = options.next();
					op = option.charAt(0);
				}
			}else{
				doubleDown(player,deck);
				//dealer will Double Down if the player does so 
				doubleDown(dealer,deck);
			}
		}
		if(op=='h'){ 
			playerHit(player, deck); 
		}
		//dealer turn
		while(dealer.countTotal()<15 && op != 'd'){
			hit(dealer, deck);
		}
	}
	// Hits
	public static void playerHit(DynamicCardArray hand, DynamicCardArray deck){ //hit for player : interactive //calls hit method
		Scanner options = new Scanner(System.in);
		String option="hit";
		char op ='h';
		while( op!='s'){
			hit(hand, deck);
			System.out.println(hand);
			System.out.println(hand.countTotal());
			if(burst(hand.countTotal())){
				break;
			}
			System.out.println("Hit again? || s for Stand h for Hit"); //prints
			option = options.next();
			op = option.charAt(0);
			while( op!='s'&& op!='h'){ //make sure the player will put h or s
				option = options.next();
				op = option.charAt(0);
			}
		}
	}
	public static void hit(DynamicCardArray hand, DynamicCardArray deck){ // regular hit
		Card card = deck.takeOut();
		hand.addCard(card);
	}
	// Double Down
	public static void doubleDown(DynamicCardArray hand, DynamicCardArray deck){
		hand.addCard(deck.takeOut());
	}
	public static void endRound(DynamicCardArray player,DynamicCardArray dealer,Bet bet, Coins coins, boolean BJ){
		
		System.out.println('\n'+"   ***Show Time***"+'\n'+"DEALER HAND:"+dealer);
		System.out.println(dealer.countTotal());
		System.out.println("PLAYER HAND:"+player);
		System.out.println(player.countTotal());
		if(BJ){
			System.out.println("Player has BlackJack!");
			bet.blackjack(coins);
		}
		else if(burst(player.countTotal())){ 
			System.out.println("Player looses - burst");
			loose(coins,bet);
		}else if(burst(dealer.countTotal())){ 
			System.out.println("Dealer looses - burst");
			win(coins,bet);
		}else if(dealer.countTotal()<player.countTotal()){ 
			System.out.println("Player Wins");
			win(coins,bet);
		}else if(dealer.countTotal()>player.countTotal()){ 
			System.out.println("Player looses");
			loose(coins,bet);
		}else{
			System.out.println("FAIR");
		}
	}
	public static boolean checkCoins(Coins coins, Bet bet){ 
		int hundreds=coins.getHundreds(); //number of 100s
		int twenties=coins.getTwenties(); //number of 20s
		int hBet=bet.getHundreds(); //number of 100s in bet
		int tBet=bet.getTwenties();	//number of 20s in bet
		if(hBet>hundreds && tBet>twenties){
			System.out.println("Number of 100s should not exceed "+hundreds+'\n'+"Number of 20s should not exceed "+twenties);
			return true;
		}else if(hBet>hundreds){
			System.out.println("Number of 100s should not exceed "+hundreds);
			return true;
		}else if(tBet>twenties){
			System.out.println("Number of 20s should not exceed "+twenties);
			return true;
		}else{
			return false;
		}
	}
	public static void win(Coins coins,Bet bet){
		int hBet=bet.getHundreds(); //number of 100s in bet
		int tBet=bet.getTwenties();	//number of 20s in bet
		coins.addCoins(hBet,tBet);
	}
	public static void loose(Coins coins,Bet bet){
		int hBet=bet.getHundreds(); //number of 100s in bet
		int tBet=bet.getTwenties(); //number of 20s in bet
		coins.reduceCoins(hBet,tBet);
	}
}
