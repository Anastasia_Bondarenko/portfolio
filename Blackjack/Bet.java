public class Bet{
	public Coins bet;
	
	public Bet(String[] numOfCoins){
		this.bet = new Coins(Integer.parseInt(numOfCoins[0]),Integer.parseInt(numOfCoins[1]));
	}
	public int getTotal(){
		return bet.getSum();
	}
	public int getHundreds(){
		return bet.getHundreds();
	}
	public int getTwenties(){
		return bet.getTwenties();
	}
	public boolean doubleDown(Coins coins){ // takes as input the players coins 
		//in Double Down, the bet increases two times
		int amount=this.bet.getSum()*2;
		if(coins.getSum()<amount){
			System.out.println("You don't have enough coins for bet, you can't DoubleDown");
			return false;
		}
		doubleBet(amount, coins);
		return true;
		
	}
	public void doubleBet(int amount, Coins coins){ //doubles the bet
		int coins100 = coins.getHundreds(); //amount of coins of 100 of the user
		int hundreds = amount/100; //coins of 100 the user will input
		hundreds=hundreds>coins100?coins100:hundreds;
		int leftOver=amount-(hundreds*100);
		int twenties = leftOver/20; //coins of 20 the user will input
		//update bet
		this.bet=new Coins(hundreds,twenties);
	}
	public void blackjack(Coins coins){
		int amount=(bet.getSum()*2); //player will be paid 200% instead of 150% because there is no 10s or 5s coins so miscalculations might be made //already tested
		int hundreds = amount/100; //coins of 100 won
		int leftOver=amount%100;
		int twenties = leftOver/20; //coins of 20 won
		//update coins
		coins.addCoins(hundreds,twenties); 
	}
}