#!/bin/bash
#Anastasia Bondarenko 2135698
#24 Avril 2022
#password.sh - Ask user to input a password many times until they hit enter
#prints the number of valid and invalid passwords

psw=a
until [[ "$psw" =~ ^$ ]]; do
	
	read -p "Enter password" psw
	if [[ "$psw" =~ ^[A-Za-z][a-zA-Z0-9]{7,15}[A-Z][^[:space:]]$ ]]; then
		valid=$((valid+1))
	else
		invalid=$((invalid+1))	
	fi
done
((invalid--))
echo Valid: $valid  Invalid: $invalid
