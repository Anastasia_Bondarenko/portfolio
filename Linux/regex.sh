
#!/bin/bash
#Anastasia Bondarenko
#20th April
#Regex.sh - Enter a file containing Strings and show if it has a match 
#to each pattern of the patterns file 
read -p "Enter a testing file" file
if [[ -e $file ]]
	
then
	IFS=$'\n'
	for line in $(cat $file); do 
		
		echo "line: $line"
		echo "Testing ends with Linux: Linux\$"
		if [[ "$line" =~ Linux$ ]]; then
			echo "'***** It is a match *****'"
		else
			echo No match
		fi
		echo "Testing starts with Linux: ^Linux"
		if [[ "$line" =~ ^Linux ]]; then
			echo "'***** It is a match *****'"
		else
			echo No match
		fi
		echo "Testing contains unix: unix"
		if [[ "$line" =~ unix ]]; then
			echo "'***** It is a match *****'"
		else
			echo No match
		fi
		echo "Testing contains unix any mixed case: [uU][nN][iI][xX]"
		if [[ "$line" =~ [uU][nN][iI][xX] ]]; then
			echo "'***** It is a match *****'"
		else
			echo No match
		fi
		echo "Testing begins with uppercase: ^[[:upper:]]"
		if [[ "$line" =~ ^[[:upper:]] ]]; then
			echo "'***** It is a match *****'"
		else
			echo No match
		fi
		echo "Testing does not contain number: ^[^[:digit:]]*$ "  #a
		if [[ "$line" =~ ^[^[:digit:]]*$ ]]; then
			echo "'***** It is a match *****'"
		else
			echo No match
		fi
		echo "Testing contains Windows,windows,Linux or linux: (Linux|linux|Windows|windows)"
		if [[ "$line" =~ (Linux|linux|Windows|windows) ]]; then
			echo "'***** It is a match *****'"
		else
			echo No match
		fi
		echo "Testing contains thingg with 2 to 5 g at the end : thingg{2,5}"
		if [[ "$line" =~ thing{2,5} ]]; then
			echo "'***** It is a match *****'"
		else
			echo No match
		fi
		echo "Testing contains no space: ^[^[:space:]]*$"  #a
		if [[ "$line" =~ ^[^[:space:]]*$ ]]; then
			echo "'***** It is a match *****'"
		else
			echo No match
		fi
		echo "Testing contains Linux with none or more x: Linux*"
		if [[ "$line" =~ Linux* ]]; then
			echo "'***** It is a match *****'"
		else
			echo No match
		fi
		echo "Testing contains Tux with blank before and after: [[:blank:]]Tux[[:blank:]]"
		if [[ "$line" =~ [[:blank:]]Tux[[:blank:]] ]]; then
			echo "'***** It is a match *****'"
		else
			echo No match
		fi
		echo "Testing ends with nix: nix$"
		if [[ "$line" =~ nix$ ]]; then
			echo "'***** It is a match *****'"
		else
			echo No match
		fi
		echo 	
	done	  

else
	echo FILE DOES NOT EXIST
	exit 5	
fi
