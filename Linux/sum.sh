
#!/bin/bash
#Anastasia Bondarenko 2135698
#05/04/2022
#sum.sh - take numbers >= 0 and displays sum, count and average of numbers

read -p "Put numbers great or equal to 0" number
echo sentinel: -1
while [[ !($number -eq -1) ]]; do
	count=$(($count+1))
	sum=$(($sum+$number))
	read -p "Put numbers great or equal to 0" number
	echo sentinel: -1
done
echo The sum is $sum, the count is $count, and the average is $(($sum/$count)) 

