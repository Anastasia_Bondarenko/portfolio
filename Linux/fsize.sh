#!/bin/bash
#Anastasia Bondarenko
#13 April 2022
#Look at the file(s) byte size in KB

for file in $@; do
	if [[ !(-f $file) ]]; then
		echo Error: not a file
		exit 2
	fi 
done

echo $(basename $0) evalating $# files

for file in $@; do
	size=$(du -b $file | cut -f1)
	echo $file size:$(($size/1024))KB 
done

