
#!/bin/bash
#Anastasia Bondarenko
#13 April 2022
#Create directories from line arguments
#Add current date and pid to the dir name after the arg
# - separated by dots

for dir in $@ ; do 
	mkdir $dir.$(date +%F).$$
done
