
import java.util.Random;

import java.util.Scanner;

class Ecosystem{
  
  /*
  *the "fire type" animal preys on the "grass type"
  *the "grass type" preys on the "water type"
  *the "water type" preys on the "fire type"
  */
  
  //types of animals on region
   public static final String FIRE_TYPE = "Charmander"; 
  
   public static final String WATER_TYPE = "Squirtle"; 
  
   public static final String GRASS_TYPE = "Fatendo";
  
   public static final String NONE = "lack of animals";
   
   //percentage of specific animal population living on the region 
  
   public static final int FIRE_PERCENT=25;
  
   public static final int WATER_PERCENT=30;
  
   public static final int GRASS_PERCENT=20;
   

  public static void main(String[]args){
     
    Scanner input=new Scanner(System.in);
    
    //takes as input time of research in hours
    System.out.println("How many hours is the reseach?");
    int hoursOfResearch=input.nextInt();
    
    //takes as input height of the region that is scanned
    System.out.println("What is the region height you're observing?");
    int regionHeight=input.nextInt();
    
    
    //populations of observed species 
    final int FIRE_POPULATION=estimatePopulation(scanForType(FIRE_TYPE, hoursOfResearch, regionHeight));
    final int GRASS_POPULATION=estimatePopulation(scanForType(GRASS_TYPE, hoursOfResearch, regionHeight));
    final int WATER_POPULATION=estimatePopulation(scanForType(WATER_TYPE, hoursOfResearch, regionHeight));
    
    //estimated days of survival for each specy 
    final int FIRETYPE_LIFESPAN=forecastLifespan(FIRE_POPULATION, WATER_POPULATION, GRASS_POPULATION);
    final int GRASSTYPE_LIFESPAN=forecastLifespan(GRASS_POPULATION, FIRE_POPULATION, WATER_POPULATION);
    final int WATERTYPE_LIFESPAN=forecastLifespan(WATER_POPULATION, GRASS_POPULATION, FIRE_POPULATION);
    
    
    
    
    //animal population and estimated survival of fire type
    System.out.println("Estimated Population of "+FIRE_TYPE+ ": " + FIRE_POPULATION);
    
    System.out.println("Lifespan Estimate of "+FIRE_TYPE+ ": " + FIRETYPE_LIFESPAN +" days");
    
    //animal population and estimated survival of water type
    System.out.println("Estimated Population of "+WATER_TYPE+ ": " + WATER_POPULATION);
    
    System.out.println("Lifespan Estimate of "+WATER_TYPE+ ": " + WATERTYPE_LIFESPAN+" days");
    
    //animal population and estimated survival of grass type
    System.out.println("Estimated Population of "+ GRASS_TYPE+ ": " + GRASS_POPULATION);
    
    System.out.println("Lifespan Estimate of "+GRASS_TYPE+ ": " + GRASSTYPE_LIFESPAN+" days");
  
  }
 
  //finds duration of one scan
  public static double getHoursOfScanning(){
    
    //a random that would make a scan duration from 0.5 hours to 2.00 hours
    Random hoursOfScan = new Random();
    double scanDurationInHours=hoursOfScan.nextDouble()*(2.0-0.5)+0.5;
    
    return scanDurationInHours;
  
  }
  //returns the type of observed animal
  public static String sampleRandomType(){
    
    //random that will generate a number from 0 to 100
    //percentage
    Random randomAnimalType = new Random();
    int animalType=randomAnimalType.nextInt(100);
    
    if(animalType<=FIRE_PERCENT){//there is 25% that the observed animal is a fire type
      
      return FIRE_TYPE;
      
    }else if(animalType>FIRE_PERCENT&&animalType<=(FIRE_PERCENT+WATER_PERCENT)){//there is 30 % that the observed animal is a water type 
   
       return WATER_TYPE;
      
    }else if(animalType>(FIRE_PERCENT+WATER_PERCENT)&&animalType<=(FIRE_PERCENT+WATER_PERCENT+GRASS_PERCENT)){//there is 20 % that the observed animal is a water type 
      
       return GRASS_TYPE;
       
    }else{
      
      return NONE;//the rest is no animal observed
    }

    
    
  }
  
  //return number of targeted animals observed in a scan
  public static int performScan(String animalType, int regionHeight){
    
    int sumTargetAnimal=0; //number of observed target animal
    
    String livingAnimal; //type of observed animal //changes many times throught a scan
     
    //loop observes the animal type in each square in each row
    //region rows
    for(int regionRow=1; regionRow<=regionHeight; regionRow++){
      
      //for region squares on each row
      for(int rowSquare=1; rowSquare<=regionRow; rowSquare++){
        
        livingAnimal=sampleRandomType(); //random animal type observed
        
        if(livingAnimal.equals(animalType)){ //checks if the observed animal type is a target animal
          
          sumTargetAnimal++; //counts observed target animals in a region scan
          
        }    
        
      //close region squares    
      }
      
      
    //close region rows  
    }
    
    return sumTargetAnimal;
    
  }
  
  //returns number of observed animals per hour
  public static double scanForType(String animalType, int scanNumberOfHours, int regionHeight){
    
    double hoursOfScan=0; //time a scan takes 
    
    double observedAnimals=0; //total number of observed studied animals in research
    
    
    while(hoursOfScan<scanNumberOfHours){//time limited to the input of number of hours of research
      
      observedAnimals+=performScan(animalType, regionHeight);//number of observed animals increases by each scan
      
      hoursOfScan+=getHoursOfScanning(); //updates duration as a sum of durations of scans
  
    }
    
    return (observedAnimals/hoursOfScan); 
    
    
  
  }
  //returns animal population as a whole number
   public static int estimatePopulation(double averageNumberOfAnimals){
    
    return (int)(averageNumberOfAnimals);
    
   }
   //updates target animal population 
   public static int updateCount(int targetAnimalPopulation, int predatorAnimalPopulation, int preyAnimalPopulation){
      
     int updateTargetAnimalPopulation=targetAnimalPopulation+preyAnimalPopulation/2-predatorAnimalPopulation;//update
     
     if (updateTargetAnimalPopulation<=0){//checks if the number is below 0
     return 0;//returns the population number when it's not negative
     }
   
     
     return updateTargetAnimalPopulation;
   }
   //returns estimated days of survival of an animal specy on a lap of 50 days
   public static int forecastLifespan(int targetAnimalPopulation, int predatorAnimalPopulation, int preyAnimalPopulation){
   
     int days=0;
     
     //50 days evaluation
     for(; days<50; days++){
       
       //animal population must be higher than 0
       if(targetAnimalPopulation>0){
         
         //takes animal populations at the begining of the day
         int targetAnimalPopulationAtBegining=targetAnimalPopulation;
         int predatorAnimalPopulationAtBegining=predatorAnimalPopulation;
         int preyAnimalPopulationAtBegining=preyAnimalPopulation;
         
         //updates target animal population at the end of the day
         targetAnimalPopulation=updateCount(targetAnimalPopulation,predatorAnimalPopulation,preyAnimalPopulation);//prey and predator population cannot be negative
         
         predatorAnimalPopulation=updateCount(predatorAnimalPopulationAtBegining,preyAnimalPopulationAtBegining,targetAnimalPopulationAtBegining);//simultanously updates predator population at the end of day
         
         preyAnimalPopulation=updateCount(preyAnimalPopulationAtBegining,targetAnimalPopulationAtBegining,predatorAnimalPopulationAtBegining);//simultanously updates prey population at the end of day
         
         continue;//restarts the for loop to update the target animal population the next day
         
       }else{
          break;//stops the for loop when the target animal didn't survive 
       }
       

     }
     return days;
     
     
  }
}
  
  
    
   

